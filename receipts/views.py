from django.shortcuts import render, get_object_or_404, redirect
from receipts.models import ExpenseCategory, Account, Receipt
from django.contrib.auth.decorators import login_required
from receipts.forms import ReceiptForm, ExpenseCategoryForm, AccountForm


# Create your views here.
@login_required
def receipt_list(request):
    receipts = Receipt.objects.filter(purchaser=request.user)
    context = {
        "receipts_list": receipts,
    }
    return render(request, "receipts/list.html", context)


@login_required
def create_receipt(request):
    if request.method == "POST":
        # We should use the form to validate the values
        # an save them to the database
        form = ReceiptForm(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.purchaser = request.user
            receipt.save()
            return redirect("home")
            # If all goes well, we can redirect the browser
            #  to another page and leave the function
    else:
        # Create an instance of the Django model form class
        form = ReceiptForm()
    # Put the form in the context
    context = {
        "form": form,
    }
    # Render the HTML template with the form
    return render(request, "receipts/create.html", context)


def account_list(request):
    accounts = Account.objects.filter(owner=request.user)
    account_counts = [
        {
            "account": account,
            "count_items": Receipt.objects.filter(
                account=account, purchaser=request.user
            ).count(),
        }
        for account in accounts
    ]
    context = {
        "account_items": account_counts,
    }
    return render(request, "receipts/account_list.html", context)


# @login_required
# def account_list(request):
#    accounts = Account.objects.filter(owner=request.user)
#    account_items = [
#        {
#            "account": account,
#            "count_items": account.receipt_set.filter(
#                purchaser=request.user
#            ).count(),
#        }
#        for account in accounts
#    ]
#    context = {
#        "account_items": account_items,
#    }
#    return render(request, "receipts/account_list.html", context)


@login_required
def category_list(request):
    categories = ExpenseCategory.objects.filter(owner=request.user)
    category_counts = [
        {
            "category": category,
            "count": Receipt.objects.filter(
                category=category, purchaser=request.user
            ).count(),
        }
        for category in categories
    ]
    context = {
        "category_counts": category_counts,
    }
    return render(request, "receipts/category_list.html", context)


# @login_required
# def category_list(request):
#    categories = ExpenseCategory.objects.filter(owner=request.user)
#    category_list_items = [
#        {
#            "category": category,
#            "count_items": category.receipt_set.filter(
#                purchaser=request.user
#            ).count(),
#        }
#        for category in categories
#    ]
#    context = {
#        "category_list_items": category_list_items,
#    }
#    return render(request, "receipts/category_list.html", context)
@login_required
def create_category(request):
    if request.method == "POST":
        form = ExpenseCategoryForm(request.POST)
        if form.is_valid():
            category = form.save(commit=False)
            category.owner = request.user
            category.save()
            return redirect("category_list")
    else:
        form = ExpenseCategoryForm()
    context = {
        "form": form,
    }
    return render(request, "receipts/create_category.html", context)


@login_required
def create_account(request):
    if request.method == "POST":
        form = AccountForm(request.POST)
        if form.is_valid():
            account = form.save(commit=False)
            account.owner = request.user
            account.save()
            return redirect("account_list")
    else:
        form = AccountForm()
    return render(request, "receipts/create_account.html", {"form": form})
